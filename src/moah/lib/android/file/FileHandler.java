package moah.lib.android.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.util.Log;

public class FileHandler {

	private Context _context;
	
	public FileHandler(Context context){
		_context = context;
	}
	
	/**
	 * Write a String to internal storage 
	 * @param filename the name of the file
	 * @param file the text that need to be stored
	 */
	public void writeFile( String filename, String file ){
		FileOutputStream outputStream;
		try {
		  outputStream = _context.openFileOutput(filename, Context.MODE_PRIVATE);
		  outputStream.write(file.getBytes());
		  outputStream.close();
		} catch (Exception e) {
		  Log.e("FileHandler", "Error writing file: " + e.toString());
		}
	}
	
	/**
	 * Write a String to internal storage 
	 * @param filename the name of the file
	 * @param file the bytes that need to be stored
	 */
	public void writeFile( String filename, byte[] file ){
		FileOutputStream outputStream;
		try {
		  outputStream = _context.openFileOutput(filename, Context.MODE_PRIVATE);
		  outputStream.write(file);
		  outputStream.close();
		} catch (Exception e) {
		  Log.e("FileHandler", "Error writing file: " + e.toString());
		}
	}
	
	/**
	 * reads the file with the specified name. Tries to return a String
	 * @param context the current Activity
	 * @param filename the name of the file
	 * @return String
	 */
	public String readFile( String filename ){
		FileInputStream fis;
		StringBuffer fileContent = new StringBuffer("");
		try {
			fis = _context.openFileInput(filename);
			byte[] buffer = new byte[1024];
			while (fis.read(buffer) != -1) {
			    fileContent.append(new String(buffer));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("FileHandler", "Could not find the file "+ filename + " error: " + e.toString());
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("FileHandler", "IOExeption while reading file error: " + e.toString());
			return null;
		}
		return fileContent.toString();
	}
	
	/**
	 * Delete the specified file
	 * @param context the current Activity
	 * @param filename the name of the file
	 */
	public void deleteFile( String filename ){
		if( fileExist(filename) ){
			_context.deleteFile(filename);
		}else{
			Log.d("FileHandler", "File doesn't exist.");
		}
	}
	
	public boolean fileExist( String filename ){
		String[] allFiles = _context.fileList();
		for (int i = 0; i < allFiles.length; i++) {
			if(filename.equals(allFiles[i])){
				return true;
			}
		}
		return false;
	}
}
