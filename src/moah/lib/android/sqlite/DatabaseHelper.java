package moah.lib.android.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper{

	private SQLiteDatabase _database;
	private String _databaseName;
	private int _version;
	
	/**
	 * Constructor
	 * 
	 * @param context the context / activity
	 * @param name	Database name
	 * @param version Version of the database
	 */
	public DatabaseHelper(Context context, String name, int version) {
		super(context, name, null, version);
		_databaseName = name;
		_version = version;
		_database = this.getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		onCreate(db);
	}
	
	/**
	 * Creates a table based on String array. Each string stands for a sql colums execution.
	 * Example sql colums execution "user_id INTEGER PRIMARY KEY AUTOINCREMENT"
	 * @param tableName Name of the table
	 * @param columns	The sql colums executions
	 */
	public void createTabel(String tableName, String[] columns){
		String execString = "CREATE TABLE IF NOT EXISTS " + tableName.toString() + " (";
		String newLine = ", ";
		String endLine = ");";
		for(int i = 0; i < columns.length; i++){
			if( (i + 1) < columns.length ){
				execString = execString + columns[i].toString() + newLine;
			}else{
				execString = execString + columns[i].toString() + endLine;
			}
		}
		Log.d("DatabaseHelper", "exec string " + execString + " in database " + _database);
		_database.execSQL( execString );
	}
	
	/**
	 * Execute SQL drop table on table name
	 * @param tableName Name of the table
	 */
	public void dropTable( String tableName ){
		_database.execSQL("DROP TABLE IF EXISTS "+ tableName);
	}
	
	/**
	 * Execute SQL drop table on table names
	 * @param tableNames The names of the tables
	 */
	public void dropTable( String[] tableNames ){
		for(int i = 0; i < tableNames.length; i++){
			dropTable(tableNames[i]);
		}
	}
	
}