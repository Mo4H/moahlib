package moah.lib.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;


public class ActivityManager {

	private static final ActivityManager INSTANCE = new ActivityManager();
	private Activity _currentActivity = null;
	public Activity getCurrentActivity(){ return _currentActivity; }
	public void setCurrentActivity(Activity activity) { _currentActivity = activity; }
	
	/**
	 * Constructor
	 */
	protected ActivityManager(){
		
	}
	
	/**
	 * Starts a activity with a intent
	 * @param context	current Activity
	 * @param intent	The intent to start a Activity
	 */
	public void startActivity(Context context, Intent intent){
		context.startActivity( intent );
	}
	
	/**
	 * Starts a activity by name. IMPORTANT Activity name must be in AndroidManifest.xml
	 * @param context	current Activity
	 * @param activityName name of the activity, like the AndroidManifest.xml
	 */
	public void startActivity(Context context, String activityName){
		Intent intent = new Intent(activityName);
		context.startActivity( intent );
	}
	
	/**
	 * Starts a activity by name. IMPORTANT Activity name must be in AndroidManifest.xml
	 * @param context	current Activity
	 * @param activityName name of the activity, like the AndroidManifest.xml
	 * @param object a object to send to he activity
	 */
	public void startActivity(Context context, String activityName, Object object){
		IntentExtended intent = new IntentExtended(activityName);
		intent.putExtra( object );
		context.startActivity( intent );
	}
	
	/**
	 * Starts a activity with a message
	 * @param context	cuurent Activity
	 * @param ActivityName Name of the Activity to be started
	 * @param params Object[]
	 */
	public void startActivity(Context context, String activityName, Object... params){
		IntentExtended intent = new IntentExtended(activityName);
		intent.putExtras( params );
		context.startActivity( intent );
	}
	
	/**
	 * Stops the activity you give in its parameters
	 * @param activity
	 */
	public void stopActivity(Activity activity){
		activity.finish();
	}
	
	/**
	 * Stops the current most top activity
	 */
	public void stopCurrentActivity(){
		this.getCurrentActivity().finish();
	}
	
	public static ActivityManager getInstance(){ return INSTANCE; }
	
}

