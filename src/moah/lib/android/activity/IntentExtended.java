package moah.lib.android.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class IntentExtended extends Intent{
	
	private Object _object;
	private Object[] _objects;
	
	public IntentExtended(){ super(); }
	
	public IntentExtended(Intent o){ super(o); }

	public IntentExtended(String action){ super(action); }
	
	public IntentExtended(String action, Uri uri){ super(action, uri); }
	
	public IntentExtended (Context packageContext, Class<?> cls){ super(packageContext, cls); }
	
	public void putExtra(Object object){ _object = object; }
	
	public void putExtras(Object[] objects){ _objects = objects; }
	
	public Object getObject(){ return _object; }
	
	public Object getObjects(){ return _objects; }
}
