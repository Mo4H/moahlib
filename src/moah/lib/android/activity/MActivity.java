package moah.lib.android.activity;

import android.app.Activity;
import android.os.Bundle;
import moah.lib.android.activity.ActivityManager;

public class MActivity extends Activity{
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    protected void onResume() {
        super.onResume();
        ActivityManager.getInstance().setCurrentActivity(this);
    }
    protected void onPause() {
        clearReferences();
        super.onPause();
    }
    protected void onDestroy() {        
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences(){
        Activity currActivity = ActivityManager.getInstance().getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)){
        	ActivityManager.getInstance().setCurrentActivity(null);
        }
    }
}
