package moah.lib.android.net;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

public class DataPost {
	
	private String url;
	/** @return the url */
	public String getUrl() { return url; }
	
	private ArrayList<NameValuePair> namePairList;
	/** @return the namePairList */
	public ArrayList<NameValuePair> getNamePairList() { return namePairList; }

	/**
	 * DataObject used for posting to an URL.
	 * @param url	URL where to post to.
	 * @param list	ArrayList<NameValuePair> the post data.
	 */
	public DataPost(String url, ArrayList<NameValuePair> list){
		this.url = url;
		this.namePairList = list;
	}
}
