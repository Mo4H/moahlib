package moah.lib.android.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class DataConnection {
	
	/**
	 * This function creates a URL request and returns the data as a String
	 * @param url The URL for the http request
	 * @param params the parameters as a List<NameValuePair>.
	 * @return String
	 */
	public String getDataFromURL(String url, List<NameValuePair> params){
		// Http request
		HttpClient client = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		String returnString = null;
		
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				returnString = readData(entity.getContent());
			} else {
				// TODO return error obj
				Log.e("DataConnection","Error statuscode "+ statusCode +"\nCheck connection or check if URL not is \"\"");
				return null;
			}
		} catch (Exception e) {
			Log.e("DataConnection","Exception @ getDataFromURL: "+ e.toString());
			return null;
		}
		return returnString;
	}
	
	/**
	 * This function creates a URL request and returns the data as a String
	 * @param url The URL for the http request
	 * @return String
	 */
	public String getDataFromURL(String url){
		return getDataFromURL(url, new ArrayList<NameValuePair>());
	}
	
	/**
	 * @param content InputStream used for the bufferedReader
	 * @return String of the InputStream reader;
	 * @throws IOException
	 */
	protected String readData(InputStream content) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = new BufferedReader( new InputStreamReader(content));
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		content.close();
		return sb.toString();
	}
}
