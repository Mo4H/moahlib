package moah.lib.android.net;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class DataService {

	private String _baseUrl = "";
	/** @return the baseUrl */
	public String getBaseUrl() { return _baseUrl; }
	/** @param baseUrl the baseUrl to set */
	public void setBaseUrl(String baseUrl) { this._baseUrl = baseUrl; }

	private JSONParser _jsonParser = null;
	
	/** The instance */
	protected static final DataService INSTANCE = new DataService();

	/** Constructor */
	protected DataService() {
		_jsonParser = new JSONParser();
	}
	
	/**
	 * Makes a httppost with a NameValuePair list to the BaseURL and retrieving the String of the url
	 * @return String data
	 */
	public String call(ArrayList<NameValuePair> list) {
		String data;
		DataCall call = new DataCall();
		DataPost post = null;
		post = new DataPost(_baseUrl, list);

		try {
			call.execute(post);
			data = call.get();
		} catch (Exception e) {
			Log.e("DataService","Exception @ Dataservice.call() returning NULL: " + e.toString());
			return null;
		}
		return data;
	}
	
	/**
	 * Makes a httppost with the NameValuePair list to the BaseURL and retrieving the JSONArray at the name of the array
	 * @param list
	 * @param jArrayName
	 * @return JSONArray
	 */
	public JSONArray callJSONArray(ArrayList<NameValuePair> list, String jArrayName) {
		return _jsonParser.createJSONArray(this.callJSONObject(list), jArrayName);
	}

	/**
	 * Makes a httppost with the NameValuePair list to the BaseURL and retrieving the JSONObject 
	 * @param list
	 * @return JSONObject
	 */
	public JSONObject callJSONObject(ArrayList<NameValuePair> list) {
		return _jsonParser.createJSONObject(this.call(list));
	}

	public boolean isConnected(Context context){
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	    return (networkInfo != null && networkInfo.isConnected());
	}	
	
	// getting the Instance
	public static DataService instance() { return INSTANCE; }
}
