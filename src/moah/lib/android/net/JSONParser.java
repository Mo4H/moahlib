package moah.lib.android.net;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONParser {

	/** Constructor */
	public JSONParser(){
	}

	/**
	 * Creates JSONObject from a String
	 * @param jsonString the input String NOTE: Must be same setup as JSON
	 * @return JSONObject NOTE: returns null if parsing Failed;
	 */
	public JSONObject createJSONObject(String jsonString) {
		JSONObject jObj = null;
		try{
			jObj = new JSONObject(jsonString);
		}catch(JSONException e){
			Log.e("JSONParser", "JSON object parsing Failed " + e.toString());
			return null;
		}
		return jObj;
	}
	
	/**
	 * Create a JSONArray from a JSONObject by a name
	 * 
	 * @param jObj the JSON object
	 * @param jsonArrayName the name of the Array name
	 * @return JSONArray
	 */
	public JSONArray createJSONArray(JSONObject jObj, String jsonArrayName) {
		JSONArray jArray = null;
		try{
			jArray = jObj.getJSONArray(jsonArrayName);
		}catch(JSONException e){
			Log.e("JSONParser", "JSON array parsing Failed " + e.toString());
			return null;
		}
		return jArray;
	}
}
