package moah.lib.android.net;

import android.os.AsyncTask;
import android.util.Log;

public class DataCall extends AsyncTask<DataPost, Void, String> {

	private DataConnection _dataConnect = null;
	
	/**
	 * Constructor
	 * 
	 * DataCall is a AsyncTask. It runs on another thread than the Main Activity.
	 * DataCall can be used like this to start a call: new DataCall().execute(new DataPost(url, NameValuePairList));
	 * and to get data u can use this: new DataCall().execute(new DataPost(url, NameValuePairList)).get();
	 * this class will return a String;
	 * NOTE: It is best to this DataService.instance().call(); Than using this class directly;
	 */
	public DataCall(){	}
	
	@Override
    protected void onPreExecute() {
		_dataConnect = new DataConnection();
    }
	
	@Override
	protected String doInBackground(DataPost... params) {
		DataPost dp = null;
		try{
			dp = params[0];
		}catch(Exception e){
			Log.e("Async", "Exception @ DataCall " + e.toString());
		}
		return _dataConnect.getDataFromURL(dp.getUrl(), dp.getNamePairList());
	}
	
	@Override
	protected void onPostExecute(String result){
		
	}
}
